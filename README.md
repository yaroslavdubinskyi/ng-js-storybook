# AngularJS Storybook Example

Based on [@storybook/html](https://github.com/storybooks/storybook/tree/master/app/html) (Alpha ver.)

## Installation

#### Install Storybook CLI aplha version globally

```
npm i -g @storybook/cli@4.0.0-alpha.10
```

#### Install local dependencies

```
npm i
```

## Start storybook dev server

```
npm run storybook
```

## Build storybook

```
npm run build-storybook
```
