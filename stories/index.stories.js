/* global document */
import { document } from 'global';
import { storiesOf } from '@storybook/html';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import angular from 'angular';

import './angular-test.component';
import angularTestHTML from './angular-test.component.html';

import './welcome.css';
import welcome from './welcome.html';

storiesOf('Sample HTML', module)
  .addDecorator(withNotes)
  .add('Welcome html element', () => welcome, { notes: 'Welcome HTML component' });

storiesOf('AngularJs', module)
  .addDecorator(withNotes)
  .add(
    'Todo component',
    () => {
      const wrapper = document.createElement('div');
      wrapper.setAttribute('id', 'ng-wrapper');
      wrapper.innerHTML = angularTestHTML;

      if (
        document.getElementById('ng-wrapper') &&
        !document.getElementById('ng-wrapper').classList.contains('ng-scope')
      ) {
        angular.bootstrap(document.getElementById('ng-wrapper'), ['todoApp']);
      } else {
        setTimeout(() => {
          angular.bootstrap(document.getElementById('ng-wrapper'), ['todoApp']);
        }, 0);
      }

      return wrapper;
    },
    { notes: 'AngularJS ToDo component' },
  );

storiesOf('Demo', module)
  .add('heading', () => '<h1>Hello World</h1>')
  .add('button', () => {
    const button = document.createElement('button');
    button.innerText = 'Hello Button';
    button.addEventListener('click', action('Click'));
    return button;
  });
